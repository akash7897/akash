import React, {Component} from "react";
class Set extends Component {
    state={
        posts: [
            {postId: 255,heading: "World Cup Semi-final",
            postedBy: "Vishal",numOfLikes: 45,numOfShares: 10,
            txt: "India lost to New Zealand in the world cup. Very Bad."},
            {postId: 377,heading: "What a final",
            postedBy: "Mohit",numOfLikes: 18,numOfShares: 4,
            txt: "Was anyone awake to see the final. England won by boundary count."},
            {postId: 391,heading: "Was it 5 runs on 6 six runs",
            postedBy: "Preeti",numOfLikes: 29,numOfShares: 7,
            txt:"I feed sorry for New Zealand. If the ball had not hit the bat and no overthrows, New Zealand would have won."},
            {postId: 417,heading: "Will Dhoni retire",
            postedBy: "Hemant",numOfLikes: 66,numOfShares: 24,
            txt:"Is this Dhoni's final match. Will be ever see Dhoni playing for India."}
        ],
        indexForLikeShare: -1,
    };
    like=(index)=>{
        let s1={...this.state};
        s1.indexForLikeShare=index;
        s1.posts[s1.indexForLikeShare].numOfLikes++;
        console.log( s1.posts[s1.indexForLikeShare].numOfLikes)
        this.setState(s1);
    }
    share=(index)=>{
        let s1={...this.state};
        s1.indexForLikeShare=index;
        s1.posts[s1.indexForLikeShare].numOfShares++;
        console.log( s1.posts[s1.indexForLikeShare].numOfShares)
        this.setState(s1);
    }
    render(){
        const {posts}=this.state;
return(
        
        <div className="container">
              {posts.map((pdt,index)=>{
                const {postId,heading,postedBy,numOfLikes,numOfShares,txt}=pdt;
              return (  
              <div className="row border bg-light">
                   <div className="col-12 border">
                       <h5>{heading}</h5><br/>
                       <h6>Shared by: {postedBy}</h6><br/>
                       {txt}
                       <br/>
                       Likes: {numOfLikes} 
                       <button className="btn btn-primary btn-sm m-1"onClick={()=>this.like(index)}>Like</button>
                       Shared: {numOfShares}
                       <button className="btn btn-primary btn-sm m-1"onClick={()=>this.share(index)}>Share</button>
                       </div>
                       </div>
              );
})}
             </div>
        
        
    )
    }
}
export default Set;