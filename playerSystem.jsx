import React, { Component } from "react";
import Player from "./player";
class PlayerSystem extends Component {
    state={
players: [
    { name: "Jack", score: 21},
    { name: "Steve", score: 30},
    { name: "Martha", score: 44},
    { name: "Bob", score: 15},
    { name: "Katherine", score: 28},
],
    };
 render(){
     const {players}=this.state;

return ( <React.Fragment>
     <h5>List of Names</h5>
    {players.map((p1)=>(
    <Player player={p1}
   />
    ))}
  
</React.Fragment>
) 
}   
}
export default PlayerSystem;